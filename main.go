package main

import (
	//"encoding/hex"
	"fmt"
	"log"
	//"io/ioutil"
	//"encoding/json"
	//"time"
	"gitlab.com/b1-201/gofare/rc522"
	"os"
	"os/signal"
)

//var zkey = []uint8{0xff, 0xff, 0xff, 0xff, 0xff, 0xff}

type Context struct {
	Scanner *rc522.Scanner
}

func main() {

	cards, err := GetCards()
	if err != nil {
		log.Fatal(err)
	}
	fmt.Printf("%s\n", cards)

	a := new(Context)
	err = a.InitScanner()
	if err != nil {
		log.Fatal(err)
	}
	defer a.Close()

	fmt.Printf("Successful InitScanner\n")
	fmt.Printf("Got version %X\n", a.Scanner.ReadRegister(rc522.RegVersion))

	go a.Listen(true)

	fmt.Printf("yay\n")

  //Exit gracefully in case of ctrl+C
	c := make(chan os.Signal, 1)
	signal.Notify(c, os.Interrupt)
	<-c
	log.Println("Closing")
}
