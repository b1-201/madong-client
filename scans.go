package main

import (
	"encoding/hex"
	"fmt"
	"gitlab.com/b1-201/gofare/rc522"
	"log"
	"time"
  "golang.org/x/crypto/sha3"
)

const (
	RP = 14 // rst pin
  AMOUNT_OF_SECTORS = 16 //Including 0
  BLOCKS_PER_SECTOR = 4 //Including 0
)
var zerokey = []byte {0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF}

func (c *Context) InitScanner() error {
	var err error
	c.Scanner, err = rc522.NewScanner(RP)
	return err
}

/*
func (c *Context) passSecretSector(muid_hash []byte) error {
  
}
*/


func (c *Context) readBlocks(blocks []uint8) (map[uint8][]uint8, error) {
  res := make(map[uint8][]uint8)
  for _, block := range blocks {
    log.Printf("læser sector %d", block)
    val, err := c.Scanner.ReadBlock(block)
    if err != nil {
      return nil, fmt.Errorf("block %d: %v", block, err)
    }
    log.Printf("sontent of block %d: %v", block, val)
    res[block] = val
  }
  return res, nil
}

func (c *Context) authBlocks(blocks []uint8) error {
  authed_sectors := make([]bool, AMOUNT_OF_SECTORS)
  var err error
  for _, block := range blocks {
    // Go rounds down
    sector := block / BLOCKS_PER_SECTOR
    // if sector not already authed, auth it, then read block
    if !authed_sectors[sector] {
      log.Printf("auther sector %d", block)
      err = c.Scanner.AuthOnDevice(rc522.CardAuthA, block, zerokey)
      if err != nil {
        return fmt.Errorf("block %d: %v", block, err)
      }
      // Mark sector as authed
      authed_sectors[sector] = true
    }
  }
  return nil
}

func (c *Context) Listen(react bool) {
	var err error
	for {
		time.Sleep(100 * time.Millisecond)
		err = c.Scanner.ConnectCard()
		if err != nil {
			continue
		}
		log.Printf("Successful card connection\n")
		log.Printf("nuid: %v\n", c.Scanner.Nuid)
		if react {
      blocks_to_auth := []uint8 {0, 6, 2, 14}
      err = c.authBlocks(blocks_to_auth)
      if err != nil {
				log.Printf("Error authing: %s\n", err.Error())
        //continue
			}
      blocks_to_read := []uint8 {0, 6, 2, 14}
      blocks_content, err := c.readBlocks2(blocks_to_read)
      if err != nil {
				log.Printf("Error reading: %s\n", err.Error())
        //continue
			}
      for _, block := range blocks_to_read {
        hex_text := hex.EncodeToString(blocks_content[block])
        fmt.Printf("Content of block %d: %s\n",block , hex_text)
      }
      
      muid_hash := make([]byte, 6)
      sha3.ShakeSum128(muid_hash, blocks_content[0])
      fmt.Printf("MUID Hash: %s\n", hex.EncodeToString(muid_hash))
      
      err = c.Scanner.AuthOnDevice(rc522.CardAuthA, 2, zerokey)
      if err != nil {
        log.Printf("Error on auth: %s\n", err.Error())
        continue
      }
      hej := []uint8{1, 2, 3, 4, 5, 6, 7, 8, 9, 1, 2, 3, 4, 5, 6, 7}
      fmt.Printf("hej")
      err = c.Scanner.WriteBlock(2, hej)
      if err != nil {
        log.Printf("Error writing: %s\n", err.Error())
      }

      c.Scanner.CloseAuth()

		}
	}

}

func (c *Context) Close() {
	c.Scanner.Close()
	return
}
