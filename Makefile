.DEFAULT_GOAL:= single

.PHONY: p single watch run watchrun

p:
	gofmt -s -w .

single:
	go build

watch:
	find . -name "*.go" | entr make single

run:
	make single && sudo ./madong-client

watchrun:
	find . -name "*.go" | entr make run
