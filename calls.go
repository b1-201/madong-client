package main

import (
	"encoding/json"
	"fmt"
	. "gitlab.com/b1-201/madong-server/pkg/api"
	"net/http"
)

func callApi(path string) (DataType, error) {
	host := "172.26.13.64" //IP address or domain
	port := "4444"
	call := "http://" + host + ":" + port + "/" + path // Change to HTTPS once ready
	fmt.Printf("%s\n\n", call)
	resp, err := http.Get(call)
	if err != nil {
		return "", err
	}

	var sresp Response
	err = json.NewDecoder(resp.Body).Decode(&sresp)
	if err != nil {
		return "", err
	}

	if sresp.Err != "" {
		return "", fmt.Errorf("server reported an error: %s", err)
	}

	return sresp.Data, nil
}

func GetCards() ([]Card, error) {
	full, err := callApi("users")
	if err != nil {
		return nil, err
	}
	//fmt.Printf("%s\n\n", full)
	var cards []Card
	err = json.Unmarshal([]byte(full), &cards)
	if err != nil {
		return nil, err
	}
	return cards, nil
}
